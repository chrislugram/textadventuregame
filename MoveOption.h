#pragma once

#include "Option.h"
#include "Room.h"

class MoveOption : public Option
{
public:
	MoveOption(Room::DIRECTIONS joiningDirection, PlayerOptions chosenOption,
		const std::string& outputText);
	~MoveOption();

	virtual bool Evaluate(const std::string& optionText, Player& player);

private:
	Room::DIRECTIONS _mDirectionToMove;
};

