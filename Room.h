#pragma once

#include "Entity.h"

class Room : public Entity
{
public:

	enum class DIRECTIONS
	{
		NORTH = 0,
		EAST,
		SOUTH,
		WEST,
		MAX
	};

	Room();
	~Room();

	void AddRoom(DIRECTIONS direction, Room* room);
	Room* GetRoom(DIRECTIONS direction) const;

private:
	Room* _mJoiningRooms[static_cast<int>(DIRECTIONS::MAX)];
};

