#include "Player.h"

#pragma region CONSTRUCTOS
Player::Player() {};

Player::Player(const string& name):_mName(name){}

Player::~Player()
{
}
#pragma endregion

#pragma region METHODS
void Player::SetName(string& name)
{
	_mName = name;
}

const string& Player::GetName() const
{
	return _mName;
}

void Player::SetCurrentRoom(const Room* room)
{
	_mCurrentRoom = room;
}

const Room* Player::GetCurrentRoom()
{
	return _mCurrentRoom;
}
#pragma endregion
