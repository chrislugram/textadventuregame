#pragma once

#include <string>
#include <iostream>
#include "Entity.h"

using namespace std;

class Room;

class Player : public Entity
{
public:
	Player();
	Player(const string& name);
	~Player();
	
	void SetName(string& name);
	const string& GetName() const;
	void SetCurrentRoom(const Room* room);
	const Room* GetCurrentRoom();

private:
	const Room* _mCurrentRoom;
	string _mName;
};

