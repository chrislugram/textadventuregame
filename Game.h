#pragma once

#include <sstream>
#include <string>
#include "Player.h"
#include "Room.h"
#include "Option.h"
#include "MoveOption.h"
#include "QuitOption.h"

class Game
{
public:

	Game();
	~Game();

	void RunGame();

private:
	static const unsigned int NUMBER_OF_OPTIONS = 5;
	static const unsigned int NUMBER_OF_ROOMS = 4;
	Room _mRooms[NUMBER_OF_ROOMS];

	Player _mPlayer;

	MoveOption _mMoveNorthOption;
	MoveOption _mMoveEastOption;
	MoveOption _mMoveSouthOption;
	MoveOption _mMoveWestOption;
	QuitOption _mQuitOption;

	Option* _mOptions[NUMBER_OF_OPTIONS];

	void InitializeRooms();
	void WelcomePlayer();
	void GivePlayerOptions() const;
	void GetPlayerInput(std::string& playerInput) const;
	PlayerOptions EvaluateInput(std::string& playerInput);
};

