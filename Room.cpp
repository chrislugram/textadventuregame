#include "Room.h"

#pragma region CONSTRUCTOS
Room::Room()
{
	for (unsigned int i = 0; i < static_cast<int>(DIRECTIONS::MAX); ++i)
	{
		_mJoiningRooms[i] = nullptr;
	}
}


Room::~Room()
{
	//delete _mJoiningRooms;
}
#pragma endregion

#pragma region METHODS
void Room::AddRoom(DIRECTIONS direction, Room* room)
{
	_mJoiningRooms[static_cast<int>(direction)] = room;
}

Room* Room::GetRoom(DIRECTIONS direction) const
{
	return _mJoiningRooms[static_cast<int>(direction)];
}
#pragma endregion

