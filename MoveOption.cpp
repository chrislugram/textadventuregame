#include "Player.h"
#include "MoveOption.h"

#pragma region CONSTRUCTORS
MoveOption::MoveOption(Room::DIRECTIONS joiningDirection, PlayerOptions chosenOption,
	const std::string& outputText)
	:Option(chosenOption, outputText)
	, _mDirectionToMove(joiningDirection)
{
}

MoveOption::~MoveOption()
{
}
#pragma endregion

#pragma region METHODS
bool MoveOption::Evaluate(const std::string& optionText, Player& player)
{
	bool handle = false;

	if (_mOptionText.compare(optionText) == 0)
	{
		const Room* pPlayerCurrentRoom = player.GetCurrentRoom();
		const Room* pNewRoom = pPlayerCurrentRoom->GetRoom(_mDirectionToMove);

		if (pNewRoom != nullptr)
		{
			player.SetCurrentRoom(pNewRoom);
			std::cout << "You have chosen to " << _mOutputText << std::endl << std::endl;
		}
		else
		{
			std::cout << "There is no room to this direction" << std::endl << std::endl;
		}

		handle = true;
	}

	return handle;
}
#pragma endregion


