#pragma once
#pragma once

#include "Option.h"

class QuitOption : public Option
{
public:
	QuitOption(const std::string& outputText);
	~QuitOption();

	inline bool ShouldQuit() const { return _mShouldQuit; }
	virtual bool Evaluate(const std::string& optionText, Player& player);

private:
	bool _mShouldQuit;
};

