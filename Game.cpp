#include "Game.h"

using namespace std;

#pragma region CONSTRUCTORS
Game::Game()
	: _mMoveNorthOption(Room::DIRECTIONS::NORTH, PlayerOptions::GO_NORTH, "Go North")
	, _mMoveEastOption(Room::DIRECTIONS::EAST, PlayerOptions::GO_EAST, "Go East")
	, _mMoveSouthOption(Room::DIRECTIONS::SOUTH, PlayerOptions::GO_SOUTH, "Go South")
	, _mMoveWestOption(Room::DIRECTIONS::WEST, PlayerOptions::GO_WEST, "Go West")
	, _mQuitOption("Quit")
{
	_mOptions[0] = dynamic_cast<Option*>(&_mMoveNorthOption);
	_mOptions[1] = dynamic_cast<Option*>(&_mMoveEastOption);
	_mOptions[2] = dynamic_cast<Option*>(&_mMoveSouthOption);
	_mOptions[3] = dynamic_cast<Option*>(&_mMoveWestOption);
	_mOptions[4] = dynamic_cast<Option*>(&_mQuitOption);
}

Game::~Game()
{
}
#pragma endregion

#pragma region METHODS
void Game::InitializeRooms()
{
	// Room 0
	_mRooms[0].AddRoom(Room::DIRECTIONS::NORTH, &(_mRooms[1]));

	// Room 1
	_mRooms[1].AddRoom(Room::DIRECTIONS::EAST, &(_mRooms[2]));
	_mRooms[1].AddRoom(Room::DIRECTIONS::SOUTH, &(_mRooms[0]));
	_mRooms[1].AddRoom(Room::DIRECTIONS::WEST, &(_mRooms[3]));

	// Room 2
	_mRooms[2].AddRoom(Room::DIRECTIONS::WEST, &(_mRooms[1]));

	// Room 3
	_mRooms[3].AddRoom(Room::DIRECTIONS::EAST, &(_mRooms[1]));

	// Set Player
	_mPlayer.SetCurrentRoom(&(_mRooms[0]));
}

void Game::WelcomePlayer()
{
	cout << "Welcome to Text Adventure!" << endl << endl;
	cout << "What is your name?" << endl << endl;

	string name;
	cin >> name;
	_mPlayer.SetName(name);

	cout << endl << "Hello " << _mPlayer.GetName() << endl;
}

void Game::GivePlayerOptions() const
{
	cout << "What would you like to do? (Enter a corresponding number)" << endl;
	
	for (unsigned int i = 0; i < NUMBER_OF_OPTIONS; ++i)
	{
		Option* option = _mOptions[i];
		const unsigned int chosenOption = (i + 1);
		cout << chosenOption << ": " << option->GetOutputText() << endl << endl;
		std::ostringstream chosenOptionString;
		chosenOptionString << chosenOption;
		option->SetOptionText(chosenOptionString.str());
	}
}

void Game::GetPlayerInput(string& playerInput) const
{
	cin >> playerInput;
}

PlayerOptions Game::EvaluateInput(string& playerInput)
{
	PlayerOptions chosenOption = PlayerOptions::NONE;

	for (unsigned int i = 0; i < NUMBER_OF_OPTIONS; ++i)
	{
		Option* option = _mOptions[i];
		bool handled = option->Evaluate(playerInput, _mPlayer);
		if (handled)
		{
			chosenOption = option->GetChosenOption();
			break;
		}
	}

	if (chosenOption == PlayerOptions::NONE)
	{
		cout << "I do not recognize that option, try again!" << endl << endl;
	}
	
	return chosenOption;
}

void Game::RunGame()
{
	InitializeRooms();

	WelcomePlayer();

	bool shouldEnd = false;
	while (!shouldEnd)
	{
		GivePlayerOptions();

		string playerInput;
		GetPlayerInput(playerInput);

		PlayerOptions selectedOption = EvaluateInput(playerInput);
		shouldEnd = (selectedOption == PlayerOptions::QUIT);
	}
}
#pragma endregion