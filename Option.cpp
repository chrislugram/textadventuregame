#include "Option.h"


#pragma region CONSTRUCTORS
Option::Option(PlayerOptions chosenOption, const std::string& outputText)
	: _mChosenOption(chosenOption)
	, _mOutputText(outputText)
{
}


Option::~Option()
{
}
#pragma endregion

#pragma region METHODS
void Option::SetOptionText(const std::string& optionText)
{
	_mOptionText = optionText;
}
#pragma endregion

