#include "QuitOption.h"

#pragma region CONSTRUCTORS
QuitOption::QuitOption(const std::string& outputText)
	: Option(PlayerOptions::QUIT, outputText)
	, _mShouldQuit(false)
{
}

QuitOption::~QuitOption()
{
}
#pragma endregion

#pragma region METHODS
bool QuitOption::Evaluate(const std::string& optionText, Player& player)
{
	_mShouldQuit = (_mOptionText.compare(optionText) == 0);

	if (_mShouldQuit)
		std::cout << "You have chosen to quit!" << std::endl << std::endl;

	return _mShouldQuit;
}
#pragma endregion

