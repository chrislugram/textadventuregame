#pragma once

#include <iostream>

class Player;

enum class PlayerOptions
{
	NONE = 0,
	GO_NORTH,
	GO_EAST,
	GO_SOUTH,
	GO_WEST,
	QUIT
};

class Option
{
public:
	Option(PlayerOptions chosenOption, const std::string& outputText);
	~Option();

	void SetOptionText(const std::string& optionText);
	inline const std::string& GetOutputText() const { return _mOutputText; }
	inline PlayerOptions GetChosenOption() const { return _mChosenOption; }

	virtual bool Evaluate(const std::string& optionText, Player& player) = 0;
protected:
	PlayerOptions _mChosenOption;
	std::string _mOutputText;
	std::string _mOptionText;
};

